-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le :  mar. 09 oct. 2018 à 15:24
-- Version du serveur :  10.1.31-MariaDB
-- Version de PHP :  7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `id7385720_onepage`
--

-- --------------------------------------------------------

--
-- Structure de la table `contenu`
--

CREATE TABLE `contenu` (
  `id` int(11) NOT NULL,
  `contenu` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastupdate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `contenu`
--

INSERT INTO `contenu` (`id`, `contenu`, `lastupdate`) VALUES
(2, '<p><strong>Le truc a implement&eacute; ck &eacute;ditor</strong></p>\r\n\r\n<p><strong><u>Du coup &ccedil;a devient coo</u></strong></p>', '2018-10-08 05:37:29'),
(3, 'Contenu milay', '2018-10-03 13:11:32'),
(4, '<p><em>Menu Titre dynamique io</em></p>', '2018-10-08 14:14:40'),
(5, '<p>Oui c&#39;est un test</p>\r\n\r\n<ul>\r\n	<li>Comme celui ci</li>\r\n	<li>Et celui ci</li>\r\n</ul>', '2018-10-08 05:39:02');

-- --------------------------------------------------------

--
-- Structure de la table `image`
--

CREATE TABLE `image` (
  `id` int(11) NOT NULL,
  `derniereModif` datetime NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `image`
--

INSERT INTO `image` (`id`, `derniereModif`, `url`) VALUES
(4, '2018-10-08 05:37:06', '5bbac2d2316a8179026325.png'),
(5, '2018-10-02 06:06:36', '5bb2e0bcb9411509644186.jpg'),
(6, '2018-10-08 14:14:40', '5bbb3c2031f97601041684.jpg'),
(7, '2018-10-08 05:39:02', '5bbac3469f222788025730.png');

-- --------------------------------------------------------

--
-- Structure de la table `mail`
--

CREATE TABLE `mail` (
  `id` int(11) NOT NULL,
  `datemail` datetime NOT NULL,
  `sender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `recipient` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lu` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `mail`
--

INSERT INTO `mail` (`id`, `datemail`, `sender`, `recipient`, `title`, `content`, `lu`) VALUES
(12, '2018-10-06 14:48:01', 'ralisonmendrika@gmail.com', 'webavance580@gmail.com', 'C\'est un test api avec le truc', 'C\'est un test', 1),
(13, '2018-10-06 16:43:12', 'ralisonmendrika@gmail.com', 'webavance580@gmail.com', 'C\'est un test api avec le truc', 'C\'est un test', 1),
(14, '2018-10-06 18:36:03', 'webavance580@gmail.com', 'ralisonmendrika@gmail.com', 'RE : C\'est un test api avec le truc', 'Envoi', 1),
(15, '2018-10-06 18:51:02', 'webavance580@gmail.com', 'ralisonmendrika@gmail.com', 'Test CK editor', '<p>Ceci est un test ck &eacute;ditor</p>\r\n\r\n<ul>\r\n	<li>Une puce&nbsp;</li>\r\n	<li>Deux puce</li>\r\n</ul>', 1),
(16, '2018-10-06 18:55:23', 'webavance580@gmail.com', 'ralisonmendrika@gmail.com', 'Test envoi email', '<p><strong><em>C&#39;est un test :</em></strong></p>\r\n\r\n<ol>\r\n	<li>Ui&nbsp;</li>\r\n	<li>LO</li>\r\n</ol>', 1),
(17, '2018-10-08 05:32:54', 'webavance580@gmail.com', 'ralisonmendrika@gmail.com', 'Titre', '<p>C&#39;est un test web</p>', 1),
(18, '2018-10-08 10:19:41', 'ralisonmendrika@gmail.com', 'webavance580@gmail.com', 'Test Template Base de donnée', 'Content', 0),
(19, '2018-10-09 07:02:40', 'test@example.com', 'webavance580@gmail.com', 'secret', 'password', 0),
(20, '2018-10-09 07:20:16', 'ralisonmendrika@gmail.com', 'webavance580@gmail.com', 'Test online', 'Test Online', 0),
(21, '2018-10-09 07:24:26', 'ralisonmendrika@gmail.com', 'webavance580@gmail.com', 'TestAPI', 'C\'est un test', 0),
(22, '2018-10-09 07:25:16', 'ralisonmendrika@gmail.com', 'webavance580@gmail.com', 'Test online 3', 'Test Online', 0),
(23, '2018-10-09 07:27:09', 'ralisonmendrika@gmail.com', 'webavance580@gmail.com', 'Test online 3', 'Test Online 3', 0);

-- --------------------------------------------------------

--
-- Structure de la table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `template_id` int(11) DEFAULT NULL,
  `content_id` int(11) DEFAULT NULL,
  `contenu` varchar(40) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `menu`
--

INSERT INTO `menu` (`id`, `template_id`, `content_id`, `contenu`) VALUES
(2, 1, 2, 'TITRA'),
(3, 2, 3, 'Menu 3'),
(4, 2, 4, 'Titre'),
(5, 2, 5, 'C\'est un test du backOffice');

-- --------------------------------------------------------

--
-- Structure de la table `menu_image`
--

CREATE TABLE `menu_image` (
  `menu_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `menu_image`
--

INSERT INTO `menu_image` (`menu_id`, `image_id`) VALUES
(2, 4),
(3, 5),
(4, 6),
(5, 7);

-- --------------------------------------------------------

--
-- Structure de la table `template`
--

CREATE TABLE `template` (
  `id` int(11) NOT NULL,
  `nom` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `urlImage` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `template`
--

INSERT INTO `template` (`id`, `nom`, `urlImage`) VALUES
(1, 'Template 1', 'examples/template1.jpg'),
(2, 'Template 2', 'examples/template2.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `username`, `password`) VALUES
(1, 'admin', 'admin');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `contenu`
--
ALTER TABLE `contenu`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `mail`
--
ALTER TABLE `mail`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_7D053A9389C2003F` (`contenu`),
  ADD KEY `IDX_7D053A935DA0FB8` (`template_id`),
  ADD KEY `IDX_7D053A9384A0A3ED` (`content_id`);

--
-- Index pour la table `menu_image`
--
ALTER TABLE `menu_image`
  ADD PRIMARY KEY (`menu_id`,`image_id`),
  ADD KEY `IDX_54912738CCD7E912` (`menu_id`),
  ADD KEY `IDX_549127383DA5256D` (`image_id`);

--
-- Index pour la table `template`
--
ALTER TABLE `template`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_97601F836C6E55B5` (`nom`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `contenu`
--
ALTER TABLE `contenu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `image`
--
ALTER TABLE `image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `mail`
--
ALTER TABLE `mail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT pour la table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `template`
--
ALTER TABLE `template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `FK_7D053A935DA0FB8` FOREIGN KEY (`template_id`) REFERENCES `template` (`id`),
  ADD CONSTRAINT `FK_7D053A9384A0A3ED` FOREIGN KEY (`content_id`) REFERENCES `contenu` (`id`);

--
-- Contraintes pour la table `menu_image`
--
ALTER TABLE `menu_image`
  ADD CONSTRAINT `FK_549127383DA5256D` FOREIGN KEY (`image_id`) REFERENCES `image` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_54912738CCD7E912` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
